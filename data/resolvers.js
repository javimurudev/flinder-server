import mongoose from 'mongoose';
import { Clientes, Usuarios } from './db';
import bcrypt from 'bcrypt';
import dotenv from 'dotenv';
import jwt from 'jsonwebtoken';

dotenv.config({path: 'variables.env'});

const crearToken = (usuarioLogin, secreto, expiresIn) => {
  const {usuario} = usuarioLogin;
  return jwt.sign(usuario, secreto, {expiresIn: expiresIn});
}


export const resolvers = {
  Query: {
    obtenerUsuario: (root, args, { usuarioActual }) => {
      if (!usuarioActual) {
        return null;
      }
      const usuario = Usuarios.findOne({email: usuarioActual.email});
      return usuario;
    }
  },
  Mutation: {
    crearUsuario: async(root, {usuario, password}) => {
      const userExist = await Usuarios.findOne({usuario});
      
      if (userExist) {
        throw Error('El usuario existe');
      }

      const newUsuario = await new Usuarios({
        usuario,
        password
      }).save();

      return "Usuario creado";
    },
    autenticarUsuario: async (root, { usuario, password }) => {      
      const nombreUsuario = await Usuarios.findOne({usuario});
      
      if (!nombreUsuario) {
        throw new Error('Usuario no encontrado');
      }

      const passwordCorrecto = await bcrypt.compare(password, nombreUsuario.password); 
      
      if (!passwordCorrecto) {
         throw new Error('Password Incorrecto');
      }
    
      return {
        token: crearToken(nombreUsuario, process.env.SECRETO, "1h")
      }

    },
    authUser: async (root, { email, password }) => { 
      console.log('authUser');
      
      const user = await User.findOne({ email: email });;
      
      if (!user) {
        throw new Error('Email does not exist');
      }
      
      const passwordIsValid = await bcrypt.compareSync(password, user.password);

      if (!passwordIsValid) {
        throw new Error('Password incorrect');
      }
  
      const token = jwt.sign({ id: user._id }, "secret");
  
      return { token, password: null, ...user._doc }
    }  
  }
}

// 
