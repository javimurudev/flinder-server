// import mongoose from 'mongoose';
const Media = require('./db').Media;
const Usuarios = require('./db').Usuarios;
const Genre = require('./db').Genre;
const bcrypt = require('bcrypt');
const dotenv = require('dotenv');
const jwt = require('jsonwebtoken');

const fs = require('fs');
// import { rejects } from 'assert';
const authenticateFacebook = require('./passport').authenticateFacebook;
const hashPassword = require('../auth/hash').hashPassword;
dotenv.config({path: 'variables.env'});
const Storage = require('@google-cloud/storage').Storage;

const gc = new Storage({
  keyFilename: "./Flinder-321d01e18042.json",
  projectId: "flinder-251816"
});

const photoFilesBucket = gc.bucket("photo-files");

const crearToken = (usuarioLogin, secreto, expiresIn) => {
  const {usuario} = usuarioLogin;
  return jwt.sign(usuario, secreto, {expiresIn: expiresIn});
}


module.exports.resolvers = {
  Query: {
    obtenerUsuario: (root, args, { usuarioActual }) => {  
      if (!usuarioActual) return null;
      return Usuarios.findOne({_id: usuarioActual.id});
    },
    obtenerUsuarios: (root, { limit }, { usuarioActual }) => {      
      if (!usuarioActual) {
        let err = new Error('Unauthorized');
        err.status = 403;
        return err;
      }      
      return Usuarios.find().limit(limit);
    },
    obtenerMedia: async (root, {limit, offset, categoriesFilter, type}, { usuarioActual }) => {     
      if (!usuarioActual) {
        let err = new Error('Unauthorized');
        err.status = 403;
        return err;
      }      

      let queryFilter = {};
      if (categoriesFilter !== undefined && categoriesFilter.length > 0) {
        queryFilter = {genre_ids: { $in: categoriesFilter}};
      }

      if (type !== undefined) {
        queryFilter = {...queryFilter, type: type};
      }      
      return Media.find(queryFilter).limit(limit).skip(offset);
    },
    obtenerSingleMedia: async (root, {id}, { usuarioActual }) => {
      if (!usuarioActual) {
        let err = new Error('Unauthorized');
        err.status = 403;
        return err;
      }

      const _id = usuarioActual.id;
      const media = await Media.findOne({_id: id});
      const userFavourites = await Usuarios.findOne({ '_id': _id }, 'favourites');       
      
       (userFavourites.favourites.includes(id))
        ? media.favourite = true
        : media.favourite = false;

      return media;
    },
    mediaCounter: async (root, { categoriesFilter, type }, { usuarioActual }) => {
      if (!usuarioActual) {
        let err = new Error('Unauthorized');
        err.status = 403;
        return err;
      }

      let queryFilter = {};
      if (categoriesFilter !== undefined && categoriesFilter.length > 0) {
        queryFilter = {genre_ids: { $in: categoriesFilter}};
      }
      
      if (type !== undefined) {
        queryFilter = {...queryFilter, type: type};
      }

      return Media.find(queryFilter).countDocuments();
    },
    search: async (root, { input }, { usuarioActual }) => {      
      const regex = new RegExp(escapeRegex(input), 'gi');
      return Media.find({title: regex},  );
    },
    obtenerGenres: async (root, { type }, { usuarioActual }) => {  
      if (!usuarioActual) {
        let err = new Error('Unauthorized');
        err.status = 403;
        return err;
      }

      let queryFilter = {};
      // if (categoriesFilter !== undefined && categoriesFilter.length > 0) {
      //   queryFilter = {genre_ids: { $in: categoriesFilter}};
      // }

      if (type !== undefined) {
        queryFilter = {...queryFilter, type: type};
      }
      
      return Genre.find(queryFilter);
    },
    obtenerUserFavourites: async (root, { type, limit, offset }, { usuarioActual }) => {
      if (!usuarioActual) {
        let err = new Error('Unauthorized');
        err.status = 403;
        return err;
      } 

      const { id } = usuarioActual;

      const media = (await Usuarios.findOne({ _id: id }).populate({ path:'favourites', options: { limit: limit, skip: offset } })).favourites
      return media;
    },
  },
  Mutation: {
    crearUsuario: async(root, {usuario, password}) => {
      const userExist = await Usuarios.findOne({usuario});
   
      if (userExist) throw Error('El usuario existe');
      // console.log(password);
      

      // console.log(password);
      
      const newUsuario = await new Usuarios({
        usuario,
        password
      }).save();

      return "Usuario creado";
    },
    createUser: async(root, {email, password, confirm}) => {
      const userExist = await Usuarios.findOne({email});
      
      if (userExist) throw Error('El usuario existe');
      
      if (password !== confirm) throw Error('Contraseñas diferentes');

      const hashedPassword = await hashPassword(password);
      const newUsuario = await new Usuarios({
        email,
        password: hashedPassword
      }).save();

      return "Useo creado";
    },
    autenticarUsuario: async (root, { usuario, password }) => {      
      const nombreUsuario = await Usuarios.findOne({usuario});
      
      if (!nombreUsuario) throw new Error('Usuario no encontrado');

      const passwordCorrecto = await bcrypt.compare(password, nombreUsuario.password); 
      
      if (!passwordCorrecto) throw new Error('Password Incorrecto');
    
      return { token: crearToken(nombreUsuario, process.env.SECRETO, "1h")} 
    },
    authUser: async (root, { email, password }) => {       
      const user = await Usuarios.findOne({ email: email });
      
      if (!user) throw new Error('Email does not exist');
      
      const passwordIsValid = await bcrypt.compareSync(password, user.password);

      if (!passwordIsValid) throw new Error('Password incorrect');
  
      const token = jwt.sign({ id: user._id }, "secret");
  
      return { token, password: null, ...user._doc }
    },
    updateMediaFavourite: async (root, { input }, { usuarioActual }) => {
      if (!usuarioActual) {
        let err = new Error('Unauthorized');
        err.status = 403;
        return err;
      }     

      const { id } = usuarioActual;
      const { mediaId, favourite } = input;
      
      if (favourite) {
        return Usuarios.findOneAndUpdate({'_id': id}, { $addToSet : {favourites: [mediaId] } },  { new : true })
          .then(() => `Media ${mediaId} added`)
          .catch(err =>err);
      } else {
        return Usuarios.findOneAndUpdate({'_id': id}, { $pull: { favourites: mediaId } } )
          .then(() => `Media ${mediaId} deleted`)
          .catch(err => err);
      }
        
    },
    deleteMedia: async (root, {id}, { usuarioActual }) => {      
      if (!usuarioActual) {
        let err = new Error('Unauthorized');
        err.status = 403;
        return err;
      }
      console.log('delete');
      

      return Media.findOneAndRemove({_id: id})
        .then(() => `Media ${id} deleted`)
        .catch(err => err);
        
    },
    updateUser: async (root, { input }, { usuarioActual }) => {  
      if (!usuarioActual) {
        let err = new Error('Unauthorized');
        err.status = 403;
        return err;
      }
      const { email } = input;
      delete input.email;
      return Usuarios.findOneAndUpdate({email: email}, input, {new : true})
        .then(() => `User ${email} updated`)
        .catch(err => err);
    },
    deleteUser: async (root, { _id }, { usuarioActual }) => {      
      if (!usuarioActual) {
        let err = new Error('Unauthorized');
        err.status = 403;
        return err;
      }
      
      // return "Prueba"
      return Usuarios.findOneAndRemove({_id: _id})
        .then(() => `User ${_id} deleted`)
        .catch(err => err);
        
    },
    authFacebook: async (root, { input }, { _req, res }) => {
      let req = {};
      const { accessToken } = input;
      req.body = {
        access_token: accessToken,
      };

      try {
        // data contains the accessToken, refreshToken and profile from passport
        const { data, info } = await authenticateFacebook(req, res);
        
        if (data) {
          const user = await Usuarios.upsertFbUser(data);

          if (user) {
            return ({
              name: user.name,
              token: user.generateJWT(),
            });
          }
        }

        if (info) {
          switch (info.code) {
            case 'ETIMEDOUT':
              return (new Error('Failed to reach Facebook: Try Again'));
            default:
              return (new Error('something went wrong'));
          }
        }
        return (Error('server error'));
      } catch (error) {
        return error;
      }
    },
    uploadFile: async (root, { file }, { usuarioActual }) => {  
      if (!usuarioActual) {
        let err = new Error('Unauthorized');
        err.status = 403;
        return err;
      }

      const f = fs.readFileSync('./README.md');
      var file = fs.createWriteStream('./README.md');

      const { stream, filename, mimetype, encoding } = await file;

      // await new Promise(res => 
      //   createReadStream
      //     .pipe(
      //       photoFilesBucket.file(filename).createWriteStream({
      //         resumable: false,
      //         gzip: true
      //       })
      //     )
      //     .on("finish", res)
      return { filename, mimetype, encoding };
    }
  }
}

function escapeRegex(text) {
  return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
};