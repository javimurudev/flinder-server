const mongoose = require ('mongoose');
// import bcrypt from 'bcrypt';
const jwt = require('jsonwebtoken');

mongoose.promise = global.Promise;

mongoose.connect(process.env.MONGO_CONNECTION, {useNewUrlParser: true});

const usuariosSchema = new mongoose.Schema({
  email: String,
  password: String,
  name: String,
  surnames: String,
  age: Number,
  description: String,
  social: {
    facebookProvider: {
        id: String,
        token: String,
    },
    googleProvider: {
        id: String,
        token: String
    }
  },
  favourites: [{
    type: Number, 
    ref: 'media', 
    default: [] 
  }]
  // favourites: [{
  //   mediaId: String
  // }]
});

const MediaSchema = new mongoose.Schema(
  { 
    original_name: {
      type: String,
      required: true
    },
    genre_ids: {
      type: [String],
      required: false
    },
    name: {
      type: String,
      required: true
    },
    popularity: {
      type: Number
    },
    origin_country: {
      type: [String]
    },
    vote_count:{
      type: Number
    },
    first_air_date: {
      type: String
    },
    backdrop_path: {
      type: String
    },
    original_language: {
      type: String
    },
    _id: {
      type: Number,
      required: true
    },
    vote_average: {
      type: Number
    },
    overview: {
      type: String
    },
    poster_path: {
      type: String
    },
    type: {
      type: String
    }
  }
);

const UserSchema = new mongoose.Schema({
  name: String,
  email: {
      type: String,
      required: true,
      unique: true
  },
  social: {
      facebookProvider: {
          id: String,
          token: String,
      },
      googleProvider: {
          id: String,
          token: String
      }
  }
});

const genreSchema = new mongoose.Schema(
  { 
    value: {
      type: String,
      required: true
    },
    label: {
      type: String,
      required: true
    },
    type: {
      type: String,
      required: true
    }
  }
);

// Model Methods
usuariosSchema.methods.generateJWT = function () {
  const today = new Date();
  const expirationDate = new Date(today);
  expirationDate.setDate(today.getDate() + 60);

  return jwt.sign({
      email: this.email,
      id: this._id,
      exp: parseInt(expirationDate.getTime() / 1000, 10),
  }, 'secret');
}

usuariosSchema.statics.upsertFbUser = async function ({ accessToken, refreshToken, profile }) {
  const User = this;

  const user = await Usuarios.findOne({ 'social.facebookProvider.id': profile.id });

  // no user was found, lets create a new one
  if (!user) {
      const newUser = await Usuarios.create({
          email: profile.emails[0].value,
          'social.facebookProvider': {
              id: profile.id,
              token: accessToken,
          },
      });

      return newUser;
  }
  return user;
};

// usuariosSchema.pre('save', function(next) {
//   // if (!this.isModified('password')) {
//   //   return next();
//   // }
//   bcrypt.genSalt(10, (err, salt) => {
//     if (err) return next(err);    
//     bcrypt.hash(this.password, salt, (err, hash) => {
//       if (err) return next(err);
//       this.password = hash;
//       next();
//     })

//   })
// })

const Usuarios = mongoose.model('usuarios', usuariosSchema);
const Media = mongoose.model('media', MediaSchema);
const Genre = mongoose.model('genre', genreSchema);

module.exports.Usuarios = Usuarios;
module.exports.Media = Media;
module.exports.Genre = Genre;
