//passport.js
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;

passport.use(new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password'
    }, 
    function (email, password, cb) {
        //this one is typically a DB call. Assume that the returned user object is pre-formatted and ready for storing in JWT
        return UserModel.findOne({email, password})
           .then(user => {
               if (!user) {
                   return cb(null, false, {message: 'Incorrect email or password.'});
               }
               return cb(null, user, {message: 'Logged In Successfully'});
          })
          .catch(err => cb(err));
    }
));



module.exports = new GoogleStrategy({
    clientID: process.env.GOOGLE_CLIENT_ID,
    clientSecret: process.env.GOOGLE_CLIENT_SECRET,
    callbackURL: process.env.GOOGLE_CALLBACK_URL
  },
  async function(email, password, cb) {
    const prevUser = await UserModel.findOne({ email: email}).exec().catch(err => cb(err, null));

    if (prevUser) {
        return cb(null, prevUser);
    }

    const user = new UserModel({
        username: profile.displayName,
        picture: profile.photos[0].value,
        socialId: profile.id
    });
    const newUser = await user.save().catch(err => cb(err, null));

    if (newUser) {
        return cb(null, newUser);
    }
  }
);