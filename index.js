const express = require('express');
const jwt = require('jsonwebtoken');

const ApolloServer = require('apollo-server-express').ApolloServer;
const typeDefs = require('./graphql/schema').typeDefs;
const resolvers = require('./graphql/resolvers').resolvers; 

// const tokenVerifier = require('./middlewares/tokenVerifier').tokenVerifier;
// const indexRouter = require('./routes/index.router');
require('./auth/passport');
const passport = require("passport");

const app = express();
const server = new ApolloServer(
  {
    typeDefs, 
    resolvers,
    context: async({req}) => {      
      const token = req.headers['authorization'];    
      // console.log('token', token);
      if (token && token !== '' && token !== null && token !== undefined) {                
        try {
          const usuarioActual = await jwt.verify(token, "secret");
          // console.log(usuarioActual);
          
          req.usuarioActual = usuarioActual;
          return {usuarioActual};
        } catch (err) {
          console.error('ERROR', err.message);
        }
      }
    },
    introspection: true, // enables introspection of the schema
    playground: true, // enables the actual playground
  }
);

// app.use('/', indexRouter);
// app.use(tokenVerifier);

server.applyMiddleware({app});

app.listen({port: 8080}, () => console.log('Running'))