const express = require('express');
const router = express.Router();

/* GET home page. */
router.get('/', function (req, res) {
  res.redirect(301, 'http://empro-ms.com');
});

module.exports = router;