import jwt from 'jsonwebtoken';

module.exports.tokenVerifier = function (req, res, next) {
  // Check header or url parameters or post parameters for token
  let token = req.body.token ||
    req.query.token ||
    req.headers['x-access-token'] ||
    req.get('authorization');

  verify(token)
    .then((decoded) => {
      // console.log(decoded);
      next();
    })
    .catch((reason) => {
      res.set('WWW-Authenticate', 'Basic realm="' + reason + '"');
      let err = new Error(reason);
      err.status = 401;
      next(err);
    });
};


const secret = process.env.JWT_secret || 'secret';

function verify(token) {
  return new Promise((resolve, reject) => {
    if (token) {
      jwt.verify(token, secret, function (err, decoded) {
        if (err) {
          return reject('Token verification failed: ' + err.message);
        }
        return resolve(decoded);
      });
    }

    reject('Authorization Required');
  });
};
